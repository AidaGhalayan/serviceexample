package com.example.serviceexample;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentSender;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

public class MyIntentService extends IntentService {

    public static final int DOWNLOAD_SUCCESS =100;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MyIntentService(String name) {
        super(MyIntentService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String path =null;
        if(intent!= null){
            path=intent.getStringExtra("url");

        }
        final ResultReceiver receiver=intent.getParcelableExtra("receiver");
        Bundle bundle= new Bundle();
        try {
            File outputFile = new File(Environment.getExternalStorageState(), "inducomicle.png");
            URL url=new URL(path);
            URLConnection urlConnection=url.openConnection();
            urlConnection.connect();
            FileOutputStream fos= new FileOutputStream(outputFile);
            InputStream inputStream=urlConnection.getInputStream();
            byte[] buffer= new byte[1024];
            int lenl=0;
            while ((lenl=inputStream.read(buffer))>0){
                fos.write(buffer, 0, lenl);
            }
            fos.close();
            inputStream.close();
            String file_path=outputFile.getPath();
            bundle.putString("file_path", file_path);
            receiver.send(DOWNLOAD_SUCCESS, bundle);


        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
