package com.example.serviceexample;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Contacts;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button start, stop;
    private Intent intentService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
//        start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ServiceResultReceiver serviceResultReceiver=new ServiceResultReceiver(new Handler(), getApplicationContext());
//                String url="https://inducesmile.com/wp-content/uploads/2049/01/inducesmilelog.png";
//                Intent intent= new Intent(MainActivity.this, MyIntentService.class);
//                intent.putExtra("receiver", serviceResultReceiver);
//                intent.putExtra("url", url);
//                startService(intent);
//
//
//            }
//        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v==start){
                    startService(new Intent(MainActivity.this, MyService.class));

                }else {
                    stopService(new Intent(MainActivity.this, MyService.class));
                }
            }

        });

    }

    private void findViews() {
        start= findViewById(R.id.start);
        stop= findViewById(R.id.stop);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        intentService=new Intent(MainActivity.this, MyIntentService.class) ;
//        intentService.putExtra("key", "hello service");
//        startService(intentService);
//        printRunningServices();
//
        }
//        private boolean isMyServiceRunning(Class<?>seviceClass){
//            ActivityManager manager=(ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
//            for(ActivityManager.RunningServiceInfo service: manager.getRunningServiceControlPanel(Integer.MAX_VALUE)){
//                if(seviceClass.getName().equals(service.service.getClassName())){
//                    return true;
//                }
//            }
//            return false;
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(intentService!=null){
            stopService(intentService);
        }
    }
}

