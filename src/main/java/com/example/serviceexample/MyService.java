package com.example.serviceexample;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class MyService extends Service {
    private MediaPlayer mediaPlayer;


    private static final String LOG_TAG = "jj";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mediaPlayer=MediaPlayer.create(getApplicationContext(), R.raw.music);
        mediaPlayer.setLooping(true);


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mediaPlayer.start();
        if (intent.hasExtra("key")){
            String text=intent.getStringExtra("key");
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        }

        return super.onStartCommand(intent, flags, startId);
        //return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.start();
    }
}
